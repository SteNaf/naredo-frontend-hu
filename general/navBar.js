class navBar {
    //This is a class that creates the nav bar
    //It has 2 parameters one for all options for the user, all pages they are allowed to acces
    //And a parameter for the username that is shown in the top right corner
    constructor(p_options, p_username) {
        this.options = p_options;
        this.username = p_username;
    }

    get getNavBar() {
        let navBar = document.createElement("header");
        navBar.id = "header";

        //The navbar contains three elements -> left, center and right
        navBar.appendChild(this.navLeft());
        navBar.appendChild(this.navCenter());
        navBar.appendChild(this.navRight());

        return navBar;
    }

    //left element
    //This is the logo and the text with the log
    navLeft() {
        let navLeft = document.createElement("div");
        navLeft.onclick = function () {
            window.location.href = "../home/home.html";
        }
        navLeft.id = "nav_left";

        let logo = document.createElement("img");
        logo.className = "logo";
        logo.src = "../general/Logo_Naredo.png";
        logo.alt = "";

        navLeft.appendChild(logo);

        let logoText = document.createElement("a");
        logoText.className = "logoText";
        logoText.innerHTML = "NAREDO";

        navLeft.appendChild(logoText);

        return navLeft;
    }

    //center element
    //This contains all the page hrefs
    navCenter() {
        let navCenter = document.createElement("div");
        navCenter.id = "nav_center";

        let burgerMenu = document.createElement("a");
        burgerMenu.innerHTML = "☰";
        burgerMenu.className = "burgerMenu";
        burgerMenu.onclick = function () {
            openBurgerMenu();
        }

        navCenter.appendChild(burgerMenu);

        let menu = document.createElement("ul");
        menu.id = "menu";

        let closeMenu = document.createElement("li");
        closeMenu.id = "closeMenu";
        closeMenu.innerHTML = "&times;";
        closeMenu.onclick = function () {
            closeBurgerMenu();
        }

        menu.appendChild(closeMenu);

        for (let i = 0; i < this.options.length; i++) {
            if (this.options[i] === null) continue;

            let optionHref = this.options[i].hrefText;

            let li = document.createElement("li");
            li.innerHTML = this.options[i].titleText;
            li.onclick = function () {
                location.href = optionHref;
            }

            menu.appendChild(li);
        }

        navCenter.appendChild(menu);

        let overlay = document.createElement("div");
        overlay.id = "overlay";
        overlay.onclick = function () {
            closeBurgerMenu();
        }

        navCenter.appendChild(overlay);

        return navCenter;
    }

    //right element
    //This contains the username and the arrow to logout
    navRight() {
        let navRight = document.createElement("div");
        navRight.id = "nav_right";

        let userName = document.createElement("a");
        userName.innerHTML = this.username.charAt(0).toUpperCase() + this.username.slice(1);
        userName.className = "logoText";

        navRight.appendChild(userName);

        let icon = document.createElement("ion-icon");
        icon.name = "log-out-outline";
        icon.className = "logout";
        icon.onclick = function () {
            logOut();
        }

        navRight.appendChild(icon);

        return navRight;
    }
}