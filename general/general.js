//Always create an AJAX request to check to which pages the user has access
//this is used to create the navigation bar
const oReq = new XMLHttpRequest();

oReq.onload = function () {
    //If user is not logged in return to login page
    if (this.responseText === "0") {
        location.href = "../index.html";
        return;
    }
    let user = JSON.parse(this.responseText);

    //Create the navbar with correct page access
    //This uses ternary
    document.getElementById("navBarContainer").appendChild(new navBar([
        user["onboarding"] !== 0 ? {
            titleText: "Onboarding",
            hrefText: "../table/table.html?type=onboarding"
        } : null, user["workout"] !== 0 ? {
            titleText: "Workout",
            hrefText: "../table/table.html?type=workout"
        } : null, user["account"] !== 0 ? {
            titleText: "Accounts",
            hrefText: "../table/table.html?type=account"
        } : {
            titleText: "Account",
            hrefText: "../form/form.html?type=account&id=" + user["User_id"]
        }], user["Role"]).getNavBar);
};
oReq.open("get", "../general/general.php", true);

oReq.send();

//Function to open the burgermenu on a screen width < 1000
function openBurgerMenu() {
    document.getElementById("menu").style.width = "100%";
    document.getElementById("overlay").style.width = "100%";
}

//Function to close the burgermenu on a screen width < 1000
function closeBurgerMenu() {
    document.getElementById("menu").style.width = "0";
    document.getElementById("overlay").style.width = "0";
}

function getUserInfo() {
    //Always create an AJAX request to check to which pages the user has access
    //this is used to create the navigation bar
    const request = new XMLHttpRequest();

    request.open("get", "../general/general.php", false);
    request.send();

    return request.responseText;
}

//function to logout
function logOut() {
    //This removes the eventlistener that is activated when you change a field in a form
    //That listener activates when you try to leave the form, and gives a dialog asking if you are sure.
    try {
        window.removeEventListener("beforeunload", unloadListener);
    } catch (e) {
    }

    location.href = '../general/logout.php';
}