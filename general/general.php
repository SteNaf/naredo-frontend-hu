<?php
//A php file that echos the user info when called
session_start();

if (!isset($_SESSION["user"])) {
    echo "0";
} else {
    echo $_SESSION["user"];
}
