class workoutTables {

    //A class for creating tables
    //A table has content, an id. A user for the permissions for the table.
    //A 'stagair' cant delete items in table etc.
    //Some type of tables can never delete items within like account
    //Value means the JSON key for the value that should be in the table. for example in a table of 'workout' the value will be 'Name_workout'
    //Value means the JSON key for the id that should be in the table. for example in a table of 'workout' the id will be 'Workout_id'
    constructor(p_tableContent, p_id, p_user, p_type, p_value, p_rowId) {
        this.tableContent = p_tableContent;
        this.id = p_id;
        this.user = p_user;
        this.type = p_type;

        this.value = p_value;
        this.rowId = p_rowId;

        //edit permission is determined per user
        this.editPermission = this.user?.["edit"];

        //duplicate permission is determined per user but for the tables of 'account' and 'onboarding' duplicating is false
        this.duplicatePermission = this.type === "account" || this.type === "onboarding" ? 0 : this.user?.["duplicate"];

        //delete permission is determined per user but for the tables of 'account' and 'onboarding' delete is false
        this.deletePermission = this.type === "account" || this.type === "onboarding" ? 0 : this.user?.["delete"];
    }

    get getTable() {
        let table = document.createElement("table");
        table.id = this.id;

        //Append the table with all the table content
        for (let i = 0; i < this.tableContent.length; i++) {
            //A row of a table contains a few things, the content for the row. Like a 'Workout naam'
            //And three buttons for edit, duplicate and delete
            let row = this.tableContent[i][this.rowId];

            //Creating the row
            let tr = document.createElement("tr");
            tr.id = row;

            //Creating the content column
            let td = document.createElement("td");
            td.innerHTML = this.tableContent[i][this.value];
            tr.appendChild(td);

            //Button column
            td = document.createElement("td");
            td.className = "editCopy"

            //If edit is possible create an edit button
            if (this.editPermission === 1) {
                td.appendChild(this.editItem(row, this.type));
            }

            //If duplicate is possible create an duplicate button
            if (this.duplicatePermission === 1) {
                td.appendChild(this.duplicateItem(this.tableContent[i], this.type));
            }

            //If delete is possible create an delete button
            if (this.deletePermission === 1) {
                td.appendChild(this.deleteItem(row, this.type));
            }

            tr.appendChild(td);

            table.appendChild(tr);
        }

        return table;
    }

    //Button for delete
    deleteItem(row, url) {
        let button = document.createElement("button");
        let icon = document.createElement("ion-icon");
        icon.name = "trash";
        icon.id = "delete";
        button.appendChild(icon);
        button.onclick = async function () {
            //Enable loading hand
            document.body.classList.toggle("wait");

            //Wait for item to be deleted
            await deleteItem(url, row);

            //Disable loading hand
            document.body.classList.toggle("wait");

            //Refresh the table
            location.href = "../table/table.html?type=" + url;
        }

        return button;
    }

    //Button for duplicate
    duplicateItem(row, url) {
        let button = document.createElement("button");
        let icon = document.createElement("ion-icon");
        icon.name = "copy-outline";
        icon.id = "copy";
        button.appendChild(icon);
        button.onclick = async function () {
            //Enable loading hand
            document.body.classList.toggle("wait");

            //Add "kopie" to duplicated name
            row.Name_workout += " (kopie)"

            //Wait for item to be duplicated
            let set = await setItem(row, url);
            set = await set.text();

            //Disable loading hand
            document.body.classList.toggle("wait");

            //If duplicate has worked go to page of duplicated item
            //Else refresh the table
            if (set !== null) {
                location.href = "../form/form.html?type=" + url + "&id=" + set;
            } else {
                location.href = "../table/table.html?type=" + url;
            }
        }

        return button;
    }

    //Edit button
    editItem(row, url) {
        let button = document.createElement("button");
        let icon = document.createElement("ion-icon");
        icon.name = "pencil-outline";
        icon.id = "edit";
        button.appendChild(icon);

        button.onclick = function () {
            //Go to form page of selected item
            location.href = "../form/form.html?type=" + url + "&id=" + row;
        }

        return button;
    }
}
