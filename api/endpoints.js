const exercisesURL = 'exercises';
const professionTypeURL = 'professiontype';
const setProcessURL = 'setprocess';
const setTypeURL = 'settype';
const workoutURL = 'workout';
const workoutFocusURL = 'workoutfocus';
const workoutGoalURL = 'workoutgoal';
const workoutLevelURL = 'workoutlevel';
const workoutTypeURL = 'workouttype';
const loginURL = "login";
const userURL = "account";
const workoutInformationURL = "workoutinformation";
const userRoleURL = "userrole";
const companyURL = "onboarding";
const industryURL = "industry";
const departmentURL = "department";

async function requestApi(url) {
    const response = await fetch('http://localhost:4567/' + url, {
        method: 'GET',
        headers: {
            'Authentication': getUserInfo()
        },
    });
    return response.json();
}

async function requestPostApi(url, settings) {
    return await fetch('http://localhost:4567/' + url, settings);
}

async function getExercises() {
    let exercise = await requestApi(exercisesURL);
    //Sort the exercises by name
    exercise.sort((a, b) => a["Name_exercise"].localeCompare(b["Name_exercise"]));
    return exercise;
}

async function getProfessionType() {
    return await requestApi(professionTypeURL);
}

async function getSetProcess() {
    return await requestApi(setProcessURL);
}

async function getSetType() {
    return await requestApi(setTypeURL);
}

async function getWorkout() {
    let workout = await requestApi(workoutURL);
    //Sort the Workout by name
    workout.sort((a, b) => a["Name_workout"].localeCompare(b["Name_workout"]));
    return workout;
}

async function getWorkoutFocus() {
    return await requestApi(workoutFocusURL);
}

async function getWorkoutGoal() {
    return await requestApi(workoutGoalURL);
}

async function getWorkoutLevel() {
    return await requestApi(workoutLevelURL);
}

async function getWorkoutType() {
    return await requestApi(workoutTypeURL);
}

async function getUsers() {
    let users = await requestApi(userURL);
    //Sort the users by Username
    users.sort((a, b) => a["Username"].localeCompare(b["Username"]))
    return users
}

async function getUserRoles() {
    return await requestApi(userRoleURL);
}

async function getWorkoutInformation() {
    return await requestApi(workoutInformationURL);
}

async function getCompany() {
    return await requestApi(companyURL);

}

async function getDepartment(companyId) {
    return await requestApi(departmentURL + "/" + companyId);
}

async function getIndustry() {
    return await requestApi(industryURL);
}

async function getItem(url) {
    return await requestApi(url);
}

//Function for simple yes or no dropdown
function getBoolean() {
    return [{
        value: "1",
        name: "Ja"
    }, {
        value: "0",
        name: "Nee"
    }];
}

//Login endpoint, only function that does not contain an 'Authentication' header
//Because it does not have one yet
async function userLogin(user) {
    try {
        return await requestPostApi(loginURL, {
            method: 'POST',
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify(user),
            mode: 'cors'
        });
    } catch (e) {
        console.log("error")
        return e;
    }
}

//A function that handles all POST requests
async function setItem(item, itemURL) {
    try {
        if (getUserInfo() === "0") return null;

        return await requestPostApi(itemURL, {
            method: 'POST',
            headers: {'Content-Type': 'application/json', 'Authentication': getUserInfo()},
            body: JSON.stringify(item),
            mode: 'cors'
        });
    } catch (e) {
        console.log("error")
        return e;
    }
}

//A function that handles all PUT requests
async function updateItem(item, itemId, itemURL) {
    try {
        if (getUserInfo() === "0") return null;

        return await requestPostApi(itemURL + "/" + itemId, {
            method: 'PUT',
            headers: {'Content-Type': 'application/json', 'Authentication': getUserInfo()},
            body: JSON.stringify(item),
            mode: 'cors'
        });
    } catch (e) {
        return e;
    }
}

//A function that handles all DELETE requests
async function deleteItem(itemURL, itemId) {
    try {
        if (getUserInfo() === "0") return null;

        return await requestPostApi(itemURL + "/" + itemId, {
            method: 'DELETE',
            headers: {'Content-Type': 'application/json', 'Authentication': getUserInfo()},
            mode: 'cors'
        });
    } catch (e) {
        return e;
    }
}