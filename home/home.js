class home {

    //Class for the home page
    //This class creates 'sok-tiles' for the page
    //Every sok-tile has a few attributes
    //The title an icon and the page it links to
    constructor(p_title, p_icon, p_href) {
        this.title = p_title;
        this.icon = p_icon;
        this.href = p_href;
    }

    get getSockTile() {
        const href = this.href;

        let sokTile = document.createElement("div");
        sokTile.className = "sok-tile";
        //Onclick to href to another page
        sokTile.onclick = function () {
            location.assign(href);
        }

        let sokTileContent = document.createElement("div");
        sokTileContent.className = "sok-tile-content";

        //Icon
        let icon = document.createElement("ion-icon");
        icon.name = this.icon;

        sokTileContent.appendChild(icon);

        sokTile.appendChild(sokTileContent);

        let sokTileName = document.createElement("div");
        sokTileName.className = "sok-tile-name";

        //Title
        let a = document.createElement("a");
        a.innerHTML = this.title;

        sokTileName.appendChild(a);

        sokTile.appendChild(sokTileName);

        return sokTile;
    }
}