class invisibleElements {
    //A class for elements that are invisible
    //It is used for elements that need to be calculated based on other elements
    //It uses a function to retrieve the data that it uses i the value parameter
    //And it has an id, in the name parameter
    constructor(p_name, p_value) {
        this.name = p_name;
        this.value = p_value;
    }

    //Get to get the object value, this is used because the elements value is calculated by a function
    get getObject() {
        const valueFunction = this.value;
        return {
            name: this.name, value: function () {
                return valueFunction();
            }
        };
    }

    //Creating an invisible element
    //This is used to make it exist in the document but not visible
    get getInvisibleElement() {
        let innerElement = document.createElement("p");
        innerElement.className = "formField";
        innerElement.style.display = "none";
        innerElement.name = this.name;
        innerElement.value = this.value;
        innerElement.innerHTML = this.value;

        return innerElement;
    }
}