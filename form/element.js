class element {

    //A class that creates elements inside the form.
    //This class includes many parameters ->

    //placeHolderText is the text that is shown as a placeholder

    //options is the array that contains all possible options for a dropdown, null when requested element is an input field
    //option_id is the key that is used to get the option id, for example if I request a dropdown to be made for 'Workout_type' an array will be sent containing the following keys
    //Workout_type_id, Workout_type_name, Set_rest, Exercise_rest; Every single option in the array has these keys and a value is bound to them
    //option_id is used to identify the correct id key, in this example 'Workout_type_id' is passed as the parameter
    //option_value is used to identify the correct Value key, in this example 'Workout_type_name' is passed as the parameter

    //id is the id of the current element these contain values like: 'FK_Workout_id' or 'fk_userRolesId'

    //type is only used when you are requesting a text field the value is either: "text" or "number" number makes it only possible to enter numbers in the text field

    //value is used to determine the current value of the field, if you are creating a new form this value is null or undefined, if you are editing an existing form this value will get the current value.
    //value will contain a single row from for example the 'Workout' table

    //required is a boolean used to determine if a field is required or not, if false fields will not turn red if field is empty, and you are trying to submit the form

    //tooltip is an array that contains all tooltips for elements
    //columnName is the key that is used to determine the correct tooltip, this is different from the id because the id can be something like 'FK_Exercise_4_process_int'
    //in that instance the column name will be 'Exercise_process_int' because the id needs to be unique and thus can contain identifying numbers

    constructor(p_placeHolderText, p_options, p_option_id, p_option_value, p_id, p_type, p_value, p_required, p_tooltip, p_columnName) {
        this.placeHolderText = p_placeHolderText;
        this.options = p_options;
        this.option_value = p_option_value;
        this.option_id = p_option_id;
        this.id = p_id;
        this.type = p_type;
        this.value = p_value;
        this.required = p_required;
        this.columnName = p_columnName;

        this.tooltipList = p_tooltip;

        //The tooltip information is calculated here, If the tooltip exists it will find the tooltip id based on the columnName
        if (this.tooltipList != null) {
            this.tooltip = this.tooltipList.find(obj => {
                return obj["Id"] === this.columnName;
            });
        } else {
            this.tooltip = null;
        }
    }

    get getDropDown() {
        let element = document.createElement("div");
        element.className = "element";

        //Every element has a header, this contains the Question that is asked and the tooltip
        element.appendChild(this.getHeader());

        let selectList = document.createElement("select");
        selectList.className = "formField";
        selectList.name = this.placeHolderText;
        selectList.id = this.id;
        selectList.required = this.required;
        selectList.placeholder = this.placeHolderText;
        selectList.oninput = function () {
            //If change set the classname back to the default value
            //If your input is wrong the className is 'formField invalid' and you want to change that back on input
            this.classList.remove("invalid");
        };

        //Placeholder is an element in a select element. A select element does not have a default attribute to define the placeholder
        //Thus we need to make an empty option with the placeholder text, this option will disappear once a different option is selected
        let placeHolder = document.createElement("option");
        placeHolder.value = "";
        placeHolder.text = this.placeHolderText;
        placeHolder.selected = true;
        placeHolder.hidden = true;

        selectList.appendChild(placeHolder);

        //Empty option is an options that does not have a value, this is used to remove the current option you selected if you change your mind
        let emptyOptions = document.createElement("option");
        emptyOptions.value = ""
        emptyOptions.text = "";
        selectList.appendChild(emptyOptions);

        //For loop to create the rest of the options
        for (let i = 0; i < this.options.length; i++) {
            let option = document.createElement("option");
            option.value = this.options[i][this.option_id];
            option.text = this.options[i][this.option_value];
            option.id = this.options[i][this.option_value];

            selectList.appendChild(option);
        }

        //If the current value and id exist set the current value to the correct value
        //You will get the value of the field by getting the row and asking for the correct key
        //If value[id] is undefined try it with the value[columName]
        if (this.value !== undefined && this.id !== undefined) {
            if (this.value[this.id] !== undefined && this.value[this.id] !== null) {
                selectList.value = this.value[this.id]
            } else if (this.value[this.columnName] !== undefined){
                selectList.value = this.value[this.columnName];
            }
        }
        element.appendChild(selectList);

        return element;
    }

    get getInputField() {
        let element = document.createElement("div");
        element.className = "element";

        //Every element has a header, this contains the Question that is asked and the tooltip
        element.appendChild(this.getHeader());

        let inputField = document.createElement("input");
        inputField.className = "formField";
        inputField.type = this.type;
        inputField.min = "0"; //Minimum value is always 0 this is only used when the type is number, meaning you can't go to negative numbers
        inputField.name = this.placeHolderText;
        inputField.id = this.id;
        inputField.placeholder = this.placeHolderText;
        inputField.required = this.required;
        inputField.value = null;

        if (this.value !== undefined && this.id !== undefined) {
            if (this.value[this.id] !== undefined) {
                inputField.value = this.value[this.id]
            } else if (this.value[this.columnName] !== undefined){
                inputField.value = this.value[this.columnName];
            } else {
                inputField.value = "";
            }
        }

        if (inputField.type === "number") {
            inputField.addEventListener("focusout", function () {
                //If the value is anything but a positive number set the value to a correct positive number
                //This is used because before this you could still copy and paste negative numbers in to the field, this will convert them to positive numbers: -50 will become 50
                inputField.value = Math.abs(parseInt(inputField.value)).toString();
            })
        }
        inputField.oninput = function () {
            //If the input field was set to invalid, aka a red field.
            //The className will be changed from 'formField invalid' back to the default class without the red or 'formField'
            this.classList.remove("invalid")
        };

        element.appendChild(inputField);

        return element;
    }

    getHeader() {
        let labelDiv = document.createElement("div");
        labelDiv.className = "labelDiv";

        let label = document.createElement("p");
        label.innerHTML = "Vul " + this.placeHolderText.toLowerCase() + " in: ";
        label.id = this.id + "_label";
        label.className = "label";

        labelDiv.appendChild(label);

        //If there is no tooltip return the header without the tooltip
        if (this.tooltip == null) return labelDiv;

        let infoDiv = document.createElement("div");
        infoDiv.className = "infoDiv";
        infoDiv.id = this.id + "_info";
        infoDiv.onclick = function (e) {
            //If anything but the Main tooltip is clicked set the display back to none
            if (e.target === this) {
                this.style.display = "none";
            }
        }

        let infoTextDiv = document.createElement("div");
        infoTextDiv.className = "infoTextDiv";

        //Header for the tooltip
        let infoHeader = document.createElement("h2");
        infoHeader.innerHTML = this.tooltip["Name"];
        infoTextDiv.appendChild(infoHeader);

        //Text for the tooltip
        let infoText = document.createElement("p");
        infoText.innerHTML = this.tooltip["Information"];
        infoText.className = "infoText";
        infoTextDiv.appendChild(infoText);

        infoDiv.appendChild(infoTextDiv);

        //Tooltips are appended to the entire body because they take up the entire screen
        document.body.appendChild(infoDiv);

        let infoIcon = document.createElement("ion-icon");
        infoIcon.name = "information-circle-outline";
        infoIcon.className = "infoIcon";
        infoIcon.onclick = function () {
            document.getElementById(infoDiv.id).style.display = "flex";
        }

        labelDiv.appendChild(infoIcon);

        return labelDiv;
    }
}