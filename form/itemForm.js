//Current tab is set to be the first tab (0)
//The type of this variable is set to 'let' because this makes it accessible to all files
let currentTab = 0;

//This creates the form based on a few parameters

//Tabs is an array containing all elements for the used tabs
//Every tab in the array is an object that contains the header and the elements of said tab

//MinimumTabsFilled is an Integer that is used to determine how many tabs should be filled in

//Type is the type of form you are creating for example 'workout' or 'user'
//This parameter is used for the requests to post or put the filled in form, with a different type value a different url will be used for the request

//Id is the current id of the form you are editing, for example you are editing 'type:id' these values are in this instance 'workout:87'

//MinimumTabsFilled is an Integer that determines whether there is a minimum for the next button, some forms use different types of next button like the 'onboarding'
//In this instance the value will not be null and the next button function will be determined in 'form.html'

function setItemForm(tabs, minimumTabsFilled, type, id, minimumTabsForNextBtn) {
    let formData = {};

    //Creating the tabs
    for (let i = 0; i < tabs.length; i++) {
        const tab = document.createElement("div");
        tab.className = "tab";
        tab.name = tabs[i].header;
        tab.id = i.toString();

        for (let j = 0; j < tabs[i].elements.length; j++) {
            let element = tabs[i].elements[j];

            //Check if element is an actual HTML element
            if (element instanceof HTMLElement) {
                tab.appendChild(element);
            } else {
                tab.appendChild(element.getInvisibleElement)
            }
        }

        document.getElementById("regForm").appendChild(tab);
    }

    //Create div for submit
    let submitDiv = document.getElementById("submit");

    //Buttons for the next and previous
    submitDiv.appendChild(makeButton("Terug", "prev", nextPrev, -1));
    submitDiv.appendChild(makeButton("Volgende", "next", nextPrev, 1));

    //This is a div that fills the space between the buttons
    //It changes the layout according to which buttons are visible
    let filler = document.createElement("div");
    filler.className = "filler";
    submitDiv.appendChild(filler);

    //Submit button
    submitDiv.appendChild(makeButton("Toepassen", "submit", mySubmit, null))

    //Function to create buttons
    function makeButton(text, id, myFunction, param) {
        let btn = document.createElement("button");
        btn.type = "button";
        btn.id = id + "Btn";
        btn.innerHTML = text;
        btn.className = id;
        btn.addEventListener("click", function () {
            myFunction(param);
        })

        return btn;
    }

    showTab(currentTab); // Display the current tab

    function showTab(n) {
        //Display the current tab
        let x = document.getElementsByClassName("tab");

        x[n].style.display = "flex";

        //Set correct header
        document.getElementById("formHeader").innerHTML = x[n].name;

        //Fix the Previous/Next/Submit buttons:
        if (n === 0) {
            document.getElementById("prevBtn").onclick = function () {
                location.href = "../table/table.html?type=" + type;
            }
        } else {
            document.getElementById("prevBtn").onclick = null;
        }

        if (n === (x.length - 1) && minimumTabsForNextBtn === null) {
            document.getElementById("nextBtn").style.display = "none";
        } else {
            document.getElementById("nextBtn").style.display = "flex";
            document.getElementById("submitBtn").style.display = "none";
        }

        //Make the submit button visible
        if (n >= minimumTabsFilled && n >= lastFilled()) {
            document.getElementById("submitBtn").style.display = "flex";
        } else {
            document.getElementById("submitBtn").style.display = "none";
        }
    }

    function nextPrev(n) {
        //Figure out which tab to display
        let x = document.getElementsByClassName("tab");
        if (x[currentTab + n] === undefined) return;

        //If any field in the current tab is empty:
        if (n === 1 && !validateForm()) return false;
        // Hide current tab:
        x[currentTab].style.display = "none";
        //Set current tab to new tab:
        currentTab = currentTab + n;
        if (currentTab < 0) currentTab = 0;

        //Display the correct tab:
        showTab(currentTab);
    }

    function validateForm() {
        // This function validates all fields in form
        let x, y, i, valid = true, isFirst = true;
        x = document.getElementsByClassName("tab");
        y = x[currentTab].querySelectorAll('input,select')

        // A loop that checks every input field in the current tab:
        for (i = 0; i < y.length; i++) {

            if (y[i].getAttribute("valid") === "false") {
                valid = false;
            }
            // If a field is empty...
            if (y[i].required && y[i].value === "") {
                y[i].className += " invalid";
                //Set the current valid status to false
                valid = false;

                //Scroll first empty field in to view.
                if (!valid && isFirst) {
                    try {
                        y[i - 1].scrollIntoView();
                    } catch (error) {
                        y[i].scrollIntoView();
                    }
                    isFirst = false;
                }
            }
        }
        return valid; //Return the valid status
    }

    function lastFilled() {
        //This function checks which field is the last filled field
        let x, y, j, i, z = 0;
        x = document.getElementsByClassName("tab");

        for (i = 0; i < x.length; i++) {
            y = x[i].getElementsByClassName("formField");
            for (j = 0; j < y.length; j++) {
                if (y[j].value !== "") {
                    z = i;
                }
            }
        }

        return z;
    }

    function mySubmit() {
        //Function for the submit button

        //This checks if all fields in tab are valid
        if (!validateForm()) return false;

        //Make confirm screen empty
        let confirmWorkout = document.getElementById("confirmWorkout");
        confirmWorkout.innerHTML = "";

        formData = {};
        //Get all form values;
        let x = document.getElementsByClassName("tab");

        //for loop to create submit screen
        for (let i = 0; i < x.length; i++) {
            //Get all fields of current tab iteration
            let y = x[i].getElementsByClassName("formField");

            let div = document.createElement("div");
            div.className = "confirmDiv";

            let h2 = document.createElement("h2");
            h2.innerHTML = x[i].name;

            div.appendChild(h2);

            let body = document.createElement("div");
            body.className = "confirmDivBody";

            for (let j = 0; j < y.length; j++) {
                //Create text for every filled in value
                let value = y[j].value;

                //Set correct value types
                if (value === "0" || parseInt(value)) {
                    value = parseInt(value);
                } else if (value === "true") {
                    value = true;
                } else if (value === "false") {
                    value = false;
                } else if (value === "") {
                    value = null;
                }
                formData[y[j].id] = value;

                let a = document.createElement("a");

                if (value === undefined || value === null) {
                    a.innerHTML = y[j].name + " = ";
                } else if (y[j] instanceof HTMLInputElement) {
                    //If element is an HTML input element
                    a.innerHTML = y[j].name + " = " + y[j].value;
                } else if (y[j] instanceof HTMLParagraphElement) {
                    //If element is an HTML paragraph element
                    //This is used for invisible elements, these elements have their own way of calculating their value and can't be changed
                    const value = tabs[i].elements[j].getObject.value()
                    formData[y[j].name] = value;
                    a.innerHTML = y[j].name + " = " + value;
                } else {
                    if (y[j].options[y[j].selectedIndex].value !== "") {
                        a.innerHTML = y[j].name + " = " + y[j].options[y[j].selectedIndex].text;
                    } else {
                        a.innerHTML = y[j].name + " = " + y[j].options[y[j].selectedIndex].value;
                    }
                }

                body.appendChild(a);
            }
            div.appendChild(body);

            confirmWorkout.appendChild(div);
        }
        //Show confirm screen
        on();
    }

    //Add a onclick function to the confirm overlay to hide the confirm div
    let confirmOverlay = document.getElementById("confirmOverlay");
    confirmOverlay.onclick=function () {
        off();
    }

    function off() {
        //Function to hide confirm screen
        confirmOverlay.style.display = "none";
        document.getElementById("confirmWorkout").style.display = "none";
    }

    function on() {
        //Get confirm screen div
        let confirmWorkout = document.getElementById("confirmWorkout");

        //Make confirm div
        let confirm = document.createElement("div");
        confirm.className = "confirm"

        //Make a cancel button
        let button = document.createElement("button");
        button.className = "cancel"
        button.innerHTML = "Terug";
        button.onclick = function () {
            //Onclick hide the current screen.
            off();
        }
        confirm.appendChild(button);

        //Make confirm button (this one actually makes a new workout or edits a workout)
        button = document.createElement("button");
        button.className = "finalSubmit";
        button.innerHTML = "Bevestigen";
        button.onclick = async function () {
            //Enable the loading hand
            document.body.classList.add("wait");

            let set;
            //If workout id exists, update a workout. Else make new workout
            if (id !== "") {
                set = await updateItem(formData, id, type);
            } else {
                set = await setItem(formData, type);
            }

            //Disable the loading hand
            document.body.classList.remove("wait");

            //If the edit or create workout was succesfull return to table
            if (set.status === 200) {
                try {
                    window.removeEventListener("beforeunload", unloadListener);
                } catch (e) {
                }

                document.location.assign("../table/table.html?type=" + type);
            } else {
                console.log("failed")
            }
        }
        confirm.appendChild(button);

        confirmWorkout.appendChild(confirm);

        //Show overlay
        confirmOverlay.style.display = "flex";

        confirmWorkout.style.display = "flex";
    }
}